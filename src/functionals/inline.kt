package functionals

inline fun modifyString(str: String, operation: (String) -> String): String { //inline requires higher order function
    return operation(str)
}

fun main(args: Array<String>) {
    modifyString("Kotlin is amazing", { it.toUpperCase() })
}