package functionals

fun main(args: Array<String>) {

    val timesTwo = { x: Int -> x*2 } // This is a "lambda" function. Kotlin infers type automatically.

    val add: (Int,Int) -> Int = { x: Int, y: Int -> x + y }

    val list = (1..100).toList()

    println(list.filter({ element -> element % 2 == 0 })) //mod 2 is used to check all even numbers, this is a more Java method.

    println(list.filter({ it % 2 == 0 }))

    println(list.filter({ it.even() }))

    list.filter { it.even() } // lambda function is only parameter, hence assumed to be last parameter

    println(list.filter(::isEven))
}

fun isEven(i: Int) = i % 2 == 0

fun Int.even() = this % 2 == 0