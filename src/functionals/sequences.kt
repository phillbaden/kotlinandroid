package functionals

import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {

    val veryLongList = (1..999999L).toList()

    var sum = 0L
    var lazySum = 0L

    val msNonLazy = measureTimeMillis {
        sum = veryLongList
                //.asSequence()       //transforms into a lazy sequence
                .filter { it > 50 }
                .map { it * 2 }
                .take(1000)         //purposeful mistake
                .sum()
    }

    val msLazy = measureTimeMillis {
        lazySum = veryLongList
                .asSequence()       //transforms into a lazy sequence
                .filter { it > 50 }
                .map { it * 2 }
                .take(1000)         //purposeful mistake, take should appear before map
                .sum()
    }

    println("Non-lazy: $msNonLazy ms, Result: $sum")
    println("Lazy: $msLazy ms, Result: $lazySum")
}