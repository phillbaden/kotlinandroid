package functionals

import java.io.FileReader

fun main(args: Array<String>) {

    FileReader("mayexist.txt").use {        //used on closable classes, essentially closing it automatically
        val str = it.readText()
        println(str)
    }

}