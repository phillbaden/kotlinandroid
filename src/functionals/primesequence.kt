package functionals

// Simple Sieve of Eratosthenes

fun main(args: Array<String>) {

    val possiblePrimesAfter2 = generateSequence(3) { it + 2 }

    val primes = generateSequence(2 to possiblePrimesAfter2) {

        // Next prime number
        val p = it.second.first()

        // Filter out elements divisible by p
        val possiblePrimesAfterP = it.second.filter { it % p != 0 }

        // Return next element in sequence
        p to possiblePrimesAfterP

    }.map { it.first }

    println(primes.take(10).toList())
}