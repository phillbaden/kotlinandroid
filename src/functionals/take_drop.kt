package functionals

fun main(args: Array<String>) {

    val list = (1..1000).toList()

    val first10 = list.take(10)
    val withoutFirst900 = list.drop(900)

    val list2 = generateSequence(0) {
        println("Calculating ${it+10}")
        it + 10
    }

    val first10List = list2.take(10).toList()
    val first20List = list2.take(20).toList()


    println(first10)
    println(withoutFirst900)
    println(first10List)
    println(first20List)
}