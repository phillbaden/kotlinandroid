package functionals

fun main(args: Array<String>) {

    val list = listOf("Hello", "to", "the", "World")
    val containsT = listOf(false, true, true, false)

    // Pair<String, Boolean>
    val zipped: List<Pair<String, Boolean>> = list.zip(containsT)

    val mapping = list.zip(list.map { it.contains("t") })

    println(zipped)
    println(mapping)
}