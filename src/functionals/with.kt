package functionals

fun main(args: Array<String>) {

    val props = System.getProperties()

    with(props) {
        list(System.out)                    //props.list not necessary due to with()
        println(propertyNames().toList())
        println(getProperty("user.home"))
    }
}