package oo

interface Driveable {
    fun drive() {
        println("Driving (interface)")
    }
}

class Bicycle : Driveable {
    override fun drive() {
        println("Driving bicycle")
    }
}

class Boat: Driveable {
    override fun drive() {
        println("Driving boat")
    }
}

fun main(args: Array<String>) {
    val driveable: Driveable = Bicycle()
    driveable.drive()
}