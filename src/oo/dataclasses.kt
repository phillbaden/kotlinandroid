package oo

// Generates a hashCode(), equals(), toString(), copy(), destructuring operator
data class Address(val street: String, val number: Int, val postCode: String, val city: String)

fun main(args: Array<String>) {
    val residence = Address("Main Street", 42, "1234", "New York")
    val residence2 = Address("Main Street", 42, "1234", "New York")

    println(residence)                  //toString()
    println(residence === residence2)   //referential equality
    println(residence == residence2)    //structural equality, equals()

    val neighbor = residence.copy(number = 43)
    println(neighbor)

    // Destructuring operator
    residence.component1()
    val (street, number, postCode, city) = residence
    println("$street $number $postCode $city")
}