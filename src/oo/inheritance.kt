package oo

// Minimal example
open class Base             //All classes are closed in Kotlin by default, hence cannot inherit unless specified open
class Child : Base()

open class Shape1(val name: String) {

    open fun area() = 0.0      //requires open if inheritance is necessary (override)
}

class Circle1(name: String, val radius: Double) : Shape1(name) {

    override fun area() = Math.PI * Math.pow(radius, 2.0)
}

fun main(args: Array<String>) {
    val smallCircle = Circle1("Small Circle", 2.0)

    println(smallCircle.name)
    println(smallCircle.radius)
    println(smallCircle.area())
}