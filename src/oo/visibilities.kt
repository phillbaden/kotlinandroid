package oo

// Private - same as in Java
// Protected - same as in Java
// Internal - visible inside same module
// no specification -> defaults to public - same as in Java

private val i = 42
fun a() = 17

open class Car(val brand: String, private val model: String) {

    protected fun tellMeYourModel() = model
}

fun main(args: Array<String>) {

    val car = Car("BRAND", "MODEL")
    car.brand
    println(i)
}