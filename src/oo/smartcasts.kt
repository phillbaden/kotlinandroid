package oo

fun Bicycle.replaceWheel() {
    println("Replacing wheel...")
}

fun Boat.startEngine(): Boolean {
    println("Starting engine...")
    return true
}

fun main(args: Array<String>) {

    val vehicle: Driveable = Bicycle()

    vehicle.drive()

    // instanceof <-> is
    if (vehicle is Bicycle) {
        vehicle.replaceWheel()
    } else if (vehicle is Boat) {
        vehicle.startEngine()
    }

    if (vehicle is Boat && vehicle.startEngine()) { //will evaluate && only if the previous statement is true
        //...
    }

    if (vehicle !is Boat || vehicle.startEngine()) {
        //....
    }

    if (vehicle !is Bicycle){
        return
    }

    vehicle.replaceWheel()

}