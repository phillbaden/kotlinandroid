package oo

class Source<out T>(val t: T) {  // Producers

    fun produceT(): T {
        return t
    }
}

class Sink<in T> {          // Consumer

    fun comsumeT(t: T) {

    }
}

fun main(args: Array<String>) {
    val strSource: Source<String> = Source("Producer")
    val anySource: Source<Any> = strSource  // out -> covariance (Source<String> is a supertype of Source<Any>)
    anySource.produceT()

    val anySink: Sink<Any> = Sink()
    val strSink: Sink<String> = anySink  // in -> contravariance (Sink<Any> is supertype of Sink<String>)
    strSink.comsumeT("Consumer")

    // Invariant = neither covariant nor contravariant

}