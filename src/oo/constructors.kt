package oo

class Country(val name: String, val areaSqKm: Int) {

    constructor(name: String) : this(name, 0) {  //A secondary constructor needs to defer to the primary constructor
        println("Constructor called")
    }

    fun print() = "$name, $areaSqKm km^2"
}

fun main(args: Array<String>) {
    val australia = Country("Australia", 7_700_000)

    println(australia.name)
    println(australia.areaSqKm)
}