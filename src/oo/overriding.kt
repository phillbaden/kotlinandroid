package oo

abstract class Vehicle {
    open fun drive() {
        println("Driving")
    }
    abstract fun honk()
}

class Sedan : Vehicle(), Driveable {

    override fun drive() {
        super<Driveable>.drive()           //super refers to parent class, however there are two parent classes
    }

    override fun honk() {
        println("Honking")
    }
}

fun main(args: Array<String>) {
    val sedan = Sedan()
    sedan.drive()
    sedan.honk()
}