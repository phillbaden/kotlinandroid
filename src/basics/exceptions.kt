package basics

import java.io.IOException

// No checked exceptions in Kotlin, implement a try, catch, finally block, much the same as in Java

fun main(args: Array<String>) {

    val input = try {
       getExternalInput()
    } catch(e: IOException) {
        e.printStackTrace()
        ""
    } finally {
       println("Finished trying to read external input")
    }

    println(input)
}

fun getExternalInput(): String {
    throw IOException("Could not read external input")
}