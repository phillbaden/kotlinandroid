package basics

fun main(args: Array<String>) {

    // map()
    val list = (1..100).toList()

    val doubled = list.map { element -> element * 2 } //map takes another function and applies it to each element of the list

    list.map{ it * 2}

    val average = list.average()
    val shifted = list.map { it - average}

    println(doubled)
    println(shifted)

    // flatMap()
    val nestedList = listOf(
            (1..10).toList(),
            (11..20).toList(),
            (21..30).toList()
    )

    val notFlattened = nestedList.map { it.sortedDescending() } // a single list with nested lists intact

    val flattened = nestedList.flatMap { it.sortedDescending() } // groups all the lists into a single list

    println(notFlattened)
    println(flattened)
}