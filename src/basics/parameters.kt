package basics

// Basic overview on named parameters

fun main(args: Array<String>) {
    val together = concat(texts = listOf("Kotlin", "Java", "Scala"), seperator = " : ")
    println(together)
}

fun concat(texts: List<String>, seperator: String = ", ") = texts.joinToString(seperator)