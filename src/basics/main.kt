package basics

fun main(args: Array<String>) {

val anonymous = "Anonymous"

println("Enter your name: ")
val input = readLine()

// Anonymous if no name is given
val name = if (input != null && input.isNotBlank()) {
    input
    } else {
    anonymous
    }

    // Message depending on input
    val message = if (name == anonymous) {
        "Privacy respected."
    } else {
        "Your IP has been sent to the authorities, $name."
    }

    println(message) // println() could also be used directly above
}