package basics

import java.util.*

// We create a collection of integers, filling it with a collection
// of random numbers between 1 and 100. We then go through this list,
// printing every one until we hit one that is less than 10, and stopping.

fun main(args: Array<String>) {

    val randoms: MutableList<Int> = mutableListOf() //can also be mutableListOf<Int>

    for (i in 1..100) {
        randoms.add(Random().nextInt(100) + 1)
    }

    var i = 0
    while (i < randoms.size && randoms[i] > 10) {
        println(randoms[i])
        i++
    }
    println(randoms[i])
}